###### First draft from ***Sécurité informatique***, Gildas Avoine

# Ethernet protocol
Insert img frame

Used localy
Connectionless : no ack, no reliability
Securityless
Error checking : CRC
48 bits (6 bytes) MAC adresses

# IP protocol
insert img packet

Connectionless
Securityless
Error checking : Checksum
32 bits (4 bytes) IP adresses
MAC adresses got no logical meaning on a Network.
IP adresses were created for that purpose.

# ICMP protocol
Implemented by default.
Mainly used to send error messages or check the machine health.
"pings" are just ICMP packets with "echo request" and "echo reply" headers.

## ICMP attacks

### ICMP big payload
Some machines can stop working when a ping > 64 Kbytes is recieved

### ICMP reflected

The attacker can broadcast on a network an ICMP packet with the victim's IP as the source IP, every machine on the network will then respond to the victim.
This attack can also be used with other protocols (ex: NTP ***Network Time Protocol***)

#### Solution

RFC-1812 : router MAY disable broadcast on an interface and MUST disable broadcast forwarding.
Still vulnerable to botnets.

# ARP protocol

Used to bind IP adresses to MAC adresses on a local network.
--> "Who has $IP ?"
<-- "Me, i'm at $MAC"
Can also be used to check IP redundancy on a same network

## ARP attacks
### ARP poisoning
Spam of unsolicited ARP answers, since some ARP implementation, for efficiency purpose, juste capture any ARP packets without sending requests everytime.
Some implementation doesn't accept unsolicited answers, but accept several answers, and keep the last. Some can also check for uncoherent answers.

# IPv6 protocol
insert img packet

128 bits (16 bytes) IP adresses
Less headers than IPv4 with a possibility of adding more
Can bring IPSec but is optional

Following protocols : NDP (ARP~), SLAAC (DHCP~)

## NDP ***Network Discovery Protocol***

Similar to ARP in a way : sending "Neighbor solicitations" and "Neighbor advertisements" with a "Neighbor cache"
With IPSec brought by IPv6, NDP can be secure. If not used, the SEND protocol can be used.

# TCP ***Transport Control Protocol***
insert img

Connectionful
Use ports for communication between applications et diffenrentiate different services.

## 3-way handshake
C --[SYN] Seq = x*--> S

C <--[SYN,ACK] Seq = y* Ack = x+1**-- S

C --[ACK] Seq = x+1 Ack = y+1**--> S

*ISN(seed)  Initial sequence number
**+1 because it has recieved or sent 1 byte

One can send [RST] or [FIN] to terminate a connection. The latter will be answered by a [FIN,ACK]

## Attacks over TCP
### Session hijacking
insert img

If an attacker can sniff a connection between a client and a server, he can then forge TCP packets using the Seq and Ack sniffed (modified accordingly with his purpose). If he sends those packets to the server, the server will answer to the client with the [ACK] of the forged packets, which the client will respond to with another [ACK] to tell the server that the numbers are false. Following an infinite loop of [ACK] sending between the twos.
If the attacker have a way to shut the client down, he could take advantage of the forged TCP connection.

### Spoofing ***from subnet***
insert img

[SYN,ACK] capture, then send the [ACK] from the machine with the forged IP

### Spoofing ***blind***
insert img

Have to guess the ISN, which can be difficult for mordern TCP implementation.

### SYN flooding

By sending multiple [SYN] packets, the server will send back [SYN,ACK] for each connection attempt, and will put in a queue the connection. The spam will cause the queue to be overload. The client only has to forge every packets with different IPs.

#### Variants
The attacker can send the forged packet with the Window flag at 0 meaning his buffer is full. The server will then send back every X times a packet to check if the windows is free.
The attacker can send the first and last bytes of the window transmission without the others. The server will then allocate his buffer to the window without ever filling it.

#### Solution

TTL for every connection half-open
FW can monitor every TCP connections and only let pass those where the 3-way-handshake is done.

# UDP ***User Datagram Protocol***
insert img

Connectionless
Use ports for communication between applications et diffenrentiate different services.

## UDP attacks

# DNS ***Domain Name System***
insert img

Each dot on a domain name involve one DNS level.
ex: ***www.domain.com*** involve 3 levels.

## DNS attacks

### DNS spoofing

Guessing of the query id (16 bits) and for modern client implementation, guessing of the source port (16 bits)

### DNS poisoning

Kaminsky's attack

#### Solution
adding random port number to the query
DNSSEC, TSIG

# NIC ***Network Interface Controler***

## Promiscuous mode
The NICs usualy filter packets that do not belong to her, in promiscuous mode, this feature is disable, allowing her to ***sniff*** every packets it can find. Can be useless on a a LAN since the switch mostly send at the right target, but pretty useful on a WLAN !

## Clear-sent protocols
telnet, rsh, http, pop, imap, smtp, ftp

# Proxy

# Firewall

## Stateful & Stateless

Stateful : keep track of the connections and the packet that should follow. Stateless : each packet is treated as alone.

## Software & Physical

Software : machine or VM which with a running OS which only role is to filter as a firewall. Physical : OS specialy design to be a firewall, happen to be more secure since it doesn't add the possible vulnerabilities of the OS.

## Application FW

Aknowledge the protocol and the corresponding port.

## Personal FW

Used on personal machines. Keep track of any application using any network and by default configured in a way that nothing goes through. This is why some app ask when launched to update the FW policies.

# IDS
* Can be signature-based. Can also be focusing on figuring out what should be a normal network activity by collecting a lots of datas, but this method will cause a lot of false positiv.
* Passiv listenning
* Work on a copy of the datas and on an isolated network (usualy behind a firewall)
## HIDS
IDS based on a single machine, which can then detect anormal user activities (privesc for example, malicious site checking, cmd history and %used, ...). Usually run as a client on the targeted machines, and send infos to a server on the isolated network.
# IPS
* Also work on signatures
* Don't work on a copy of the datas
* Activ listening -> can take actions (block, timeout, ...)

## HIPS
Can be compared to AntiViruses. However, advanced AV are now called "Security suites", and refer to HIPS for regular customer.